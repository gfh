# phpMyAdmin MySQL-Dump
# version 2.2.3
# http://phpwizard.net/phpMyAdmin/
# http://phpmyadmin.sourceforge.net/ (download page)
#
# Host: localhost
# Generation Time: Feb 23, 2002 at 10:46 PM
# Server version: 3.23.41
# PHP Version: 4.0.6
# Database : `mb`
# --------------------------------------------------------

#
# Table structure for table `boardcat`
#

CREATE TABLE boardcat (
  id bigint(20) NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  theorder bigint(20) NOT NULL default '0',
  PRIMARY KEY  (id),
  UNIQUE KEY id (id)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `boards`
#

CREATE TABLE boards (
  boardid bigint(10) NOT NULL auto_increment,
  boardname mediumtext NOT NULL,
  boardshow tinyint(1) NOT NULL default '0',
  boardlevel tinyint(2) NOT NULL default '0',
  type tinyint(1) NOT NULL default '0',
  clan tinyint(4) NOT NULL default '0',
  caption mediumtext NOT NULL,
  theorder bigint(20) NOT NULL default '0',
  PRIMARY KEY  (boardid),
  UNIQUE KEY boardid (boardid),
  KEY type (type)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `favorites`
#

CREATE TABLE favorites (
  fid bigint(20) NOT NULL auto_increment,
  fboard bigint(20) NOT NULL default '0',
  fuser bigint(20) NOT NULL default '0',
  PRIMARY KEY  (fid),
  UNIQUE KEY fid (fid),
  KEY fuser (fuser)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `levels`
#

CREATE TABLE levels (
  id bigint(20) NOT NULL auto_increment,
  levnum bigint(20) NOT NULL default '0',
  caption mediumtext NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY id (id)
) TYPE=MyISAM;
INSERT INTO levels VALUES (1, 0, '<b>0: Unconfirmed</b><br>User has not confirmed his account.  Cannot read or post messages.');
INSERT INTO levels VALUES (2, 5, '<b>5: Asshole</b><br>This user is an asshole.  He cannot post anywhere meaningful.  I hope he dies.');
INSERT INTO levels VALUES (3, 20, '<b>20: Normal User</b><br>User has aquired 20 aura or more.  Has access to the Normal User\\\'s Board.');
INSERT INTO levels VALUES (4, 15, '<b>15: New User</b><br>User has less than 20 aura.');
INSERT INTO levels VALUES (5, 31, '<b>31: Intelligent User</b><br>User has aquired 50 aura or more.');
INSERT INTO levels VALUES (6, 32, '<b>32: Legend</b><br>User is a legend.  Has aquired over 500 aura.  This user really kicks ass.');
INSERT INTO levels VALUES (7, 50, '<b>50: Moderator</b><br>User can delete messages and suspend user\\\'s posting privelages.');
INSERT INTO levels VALUES (8, 60, '<b>60: Administrator</b><br>User has full control over all aspects of the message boards.');
# --------------------------------------------------------

#
# Table structure for table `marked`
#

CREATE TABLE marked (
  markid bigint(10) NOT NULL auto_increment,
  reason tinyint(1) NOT NULL default '0',
  markwho mediumtext NOT NULL,
  message bigint(10) NOT NULL default '0',
  topic bigint(10) NOT NULL default '0',
  board bigint(10) NOT NULL default '0',
  active tinyint(1) NOT NULL default '0',
  reason2 mediumtext NOT NULL,
  markedtimes bigint(10) NOT NULL default '0',
  PRIMARY KEY  (markid),
  UNIQUE KEY mardid (markid),
  KEY active (active)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `messages`
#

CREATE TABLE messages (
  messageid bigint(10) NOT NULL auto_increment,
  topic bigint(10) NOT NULL default '0',
  messby mediumtext NOT NULL,
  messsec bigint(40) NOT NULL default '0',
  messbody longtext NOT NULL,
  mesboard bigint(10) NOT NULL default '0',
  theorder bigint(10) NOT NULL default '0',
  postdate mediumtext NOT NULL,
  auraed tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (messageid),
  UNIQUE KEY messageid (messageid),
  KEY topic (topic),
  KEY mesboard (mesboard)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `modded`
#

CREATE TABLE modded (
  modid bigint(10) NOT NULL auto_increment,
  modbod longtext NOT NULL,
  board bigint(10) NOT NULL default '0',
  topic bigint(10) NOT NULL default '0',
  message bigint(10) NOT NULL default '0',
  modby mediumtext NOT NULL,
  moddedat mediumtext NOT NULL,
  postedat mediumtext NOT NULL,
  PRIMARY KEY  (modid),
  UNIQUE KEY modid (modid)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `moderators`
#

CREATE TABLE moderators (
  id bigint(20) NOT NULL auto_increment,
  modby varchar(255) NOT NULL default '',
  modid bigint(20) NOT NULL default '0',
  type varchar(255) NOT NULL default '',
  action varchar(255) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `suggest`
#

CREATE TABLE suggest (
  sugid bigint(10) NOT NULL auto_increment,
  reason tinyint(1) NOT NULL default '0',
  sugwho mediumtext NOT NULL,
  message bigint(10) NOT NULL default '0',
  topic bigint(10) NOT NULL default '0',
  board bigint(10) NOT NULL default '0',
  active tinyint(1) NOT NULL default '0',
  reason2 mediumtext NOT NULL,
  sugtimes tinyint(10) NOT NULL default '0',
  PRIMARY KEY  (sugid),
  UNIQUE KEY mardid (sugid),
  KEY active (active)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `systemnot`
#

CREATE TABLE systemnot (
  sysnotid bigint(10) NOT NULL auto_increment,
  sysbod mediumtext NOT NULL,
  sendto bigint(10) NOT NULL default '0',
  sentat mediumtext NOT NULL,
  sentfrom tinyint(1) NOT NULL default '0',
  active tinyint(1) NOT NULL default '0',
  userid bigint(20) NOT NULL default '0',
  PRIMARY KEY  (sysnotid)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `topics`
#

CREATE TABLE topics (
  topicid bigint(10) NOT NULL auto_increment,
  topicname mediumtext NOT NULL,
  topicby mediumtext NOT NULL,
  boardnum bigint(10) NOT NULL default '0',
  active tinyint(1) NOT NULL default '0',
  timesec bigint(40) NOT NULL default '0',
  closed tinyint(1) NOT NULL default '0',
  postdate mediumtext NOT NULL,
  PRIMARY KEY  (topicid),
  UNIQUE KEY topicid (topicid),
  KEY boardnum (boardnum)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `usermap`
#

CREATE TABLE usermap (
  mapid bigint(10) NOT NULL auto_increment,
  userid1 bigint(10) NOT NULL default '0',
  userid2 bigint(10) NOT NULL default '0',
  PRIMARY KEY  (mapid),
  UNIQUE KEY mapid (mapid)
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `users`
#

CREATE TABLE users (
  userid bigint(10) NOT NULL auto_increment,
  level bigint(2) NOT NULL default '0',
  username varchar(30) NOT NULL default '',
  userpass varchar(255) NOT NULL default '',
  cookies bigint(10) NOT NULL default '0',
  lastsid mediumtext NOT NULL,
  poststoday bigint(10) NOT NULL default '0',
  topicstoday bigint(10) NOT NULL default '0',
  regsid mediumtext NOT NULL,
  email mediumtext NOT NULL,
  regdate mediumtext NOT NULL,
  lastactivity mediumtext NOT NULL,
  notify tinyint(1) NOT NULL default '0',
  regsec bigint(90) NOT NULL default '0',
  lastsec bigint(90) NOT NULL default '0',
  faction bigint(10) NOT NULL default '0',
  lastacip mediumtext NOT NULL,
  money bigint(90) NOT NULL default '0',
  turns bigint(20) NOT NULL default '0',
  attack bigint(20) NOT NULL default '0',
  attacked tinyint(4) NOT NULL default '0',
  attackout tinyint(4) NOT NULL default '0',
  speclev mediumtext NOT NULL,
  colorsel tinyint(4) NOT NULL default '1',
  email2 varchar(50) NOT NULL default '',
  PRIMARY KEY  (userid),
  UNIQUE KEY userid (userid),
  KEY username (username),
  KEY userpass (userpass)
) TYPE=MyISAM;

